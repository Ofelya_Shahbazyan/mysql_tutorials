-- ----------------------------
-- 1.
-- ----------------------------
delimiter //
CREATE PROCEDURE mailingListCount ( OUT v_rows INTEGER) 
BEGIN   
  SELECT COUNT(*) INTO v_rows   
  FROM Customers    
  WHERE NOT cust_email IS NULL;   
END//
    


delimiter ;
call mailingListCount(@ReturnValue);
select @ReturnValue;



